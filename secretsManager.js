const {SecretsManagerClient, GetSecretValueCommand} = require('@aws-sdk/client-secrets-manager');

module.exports = () => {
    const secret_name = "ADABounties-Mainnet";
    const client = new SecretsManagerClient({
         region: "us-east-1",
    });
    return new Promise((resolve, reject) => {
        client.send(
            new GetSecretValueCommand({
                SecretId: secret_name,
                VersionStage: "AWSCURRENT", // VersionStage defaults to AWSCURRENT if unspecified
            })).then(data => {
            //parsing the fetched data into JSON
            const secretsJSON = JSON.parse(data.SecretString);

            // creating a string to store write to .env file
            // .env file shall look like this :
            // SECRET_1 = sample_secret_1
            // SECRET_2 = sample_secret_2
            let secretsString = "";
            Object.keys(secretsJSON).forEach((key) => {
                secretsString += `${key}=${secretsJSON[key]}\n`;
            });
            resolve(secretsString);
        }).catch(err => {
            console.error("Error occurred while trying to pull the Secrets! [" + err + "]");
            reject(err);
        });
    });
};
