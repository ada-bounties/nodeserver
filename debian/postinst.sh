#!/bin/bash
rm -fr /opt/nodeserver/*
mkdir -p /opt/nodeserver/logs
cp -r /usr/share/adabounties-nodeserver/app/dist/* /opt/nodeserver
cp /usr/share/adabounties-nodeserver/app/dist/adabounties-nodeserver.service /etc/systemd/system/adabounties-nodeserver.service
chown -R ubuntu /opt/nodeserver
