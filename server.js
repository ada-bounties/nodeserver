const express = require('express');
const Cardano = require('@emurgo/cardano-serialization-lib-nodejs');
const axios = require("axios");
const {promises: fs} = require("fs");
const path = require("path");
const port = 8091;
const txMap = new Map();
const txSendAmountMap = new Map();
const txTimeMap = new Map();
const ttl = 600;
let url;
const retrieveSecrets = require("./secretsManager");
const app = express();
app.use(express.urlencoded({extended: true, limit: "10mb", parameterLimit: 50}));

app.put('/resubmitTx', (req, res) => {
    const wla = req.query.wla;
    const submitUrl = req.query.submitUrl;
    const oldTxHash = req.query.txHash;
    const cnftRaw = req.query.txJson;
    try {
        const tx = Cardano.Transaction.from_json(cnftRaw);
        axios({
            method: "post",
            url: submitUrl,
            data: Buffer.from(tx.to_bytes(), "utf8"),
            headers: {
                "Content-Type": "application/cbor"
            }
        }).then(result => {
            console.log("Resubmit success! [" + wla + "][" + result.data + "]");
            res.end(result.data);
        }).catch(e => {
            console.log("RESUBMIT - ERROR submitting to [" + submitUrl + "] [" + e + "]");
            console.log(e);
            let errorMsg = "RESUBMITERROR";
            if ((e != null) && (e.response != null) && (((e.response.status != null) && ((e.response.status === 400) || (e.response.status === 404)))
                || ((e.response.statusText != null) && (e.response.statusText === 'Not Found')))) {
                errorMsg = "404";
            } else if ((e != null) && (e.code != null) && (e.code === 'ECONNRESET')) {
                errorMsg = "ECONNRESET";
            }
            res.end(errorMsg);
        });
    } catch (e) {
        console.log(e);
        res.end("RESUBMITERROR");
    }
});

app.put('/createTx', (req, res) => {
    try {
        console.log(req.query);
        const paymentAddress = req.query.paymentAddress;
        const sendAmount = +req.query.sendAmount;
        let txSendAmount = sendAmount;
        const sendPadding = 1500000;
        let amtCheck = sendPadding + sendAmount;
        const token = req.query.token;
        const tokenSubject = req.query.tokenSubject;
        let hexPolicyId = '';
        let hexTokenName = '';
        let assetName;
        if (token !== 'ADA') {
            hexPolicyId = tokenSubject.substring(0, 56);
            hexTokenName = tokenSubject.substring(56);
            assetName = Cardano.AssetName.new(Buffer.from(hexTokenName, "hex"));
        }
        const coinsPerUtxoWord = req.query.coinsPerUtxoWord;
        const slot = req.query.slot;
        const minFeeA = req.query.minFeeA;
        const minFeeB = req.query.minFeeB;
        const keyDeposit = req.query.keyDeposit;
        const poolDeposit = req.query.poolDeposit;
        const maxTxSize = req.query.maxTxSize;
        const maxValSize = req.query.maxValSize;
        const changeAdd = req.query.changeAdd;
        const bountyIds = req.query.bountyIds.split(",");
        let claimId = req.query.claimId;
        const hashes = req.query.hashes.split(",");
        if (claimId === '') {
            txMap.delete(bountyIds[0]);
            txTimeMap.delete(bountyIds[0]);
            txSendAmountMap.delete(bountyIds[0]);
        } else {
            txMap.delete(claimId);
            txTimeMap.delete(claimId);
            txSendAmountMap.delete(claimId);
        }
        console.log("Claim Id [" + claimId + "]");
        const bountyUrls = [];
        for (let i = 0; i < bountyIds.length; ++i) {
            console.log("Bounty Id [" + bountyIds[i] + "]");
            const bountyUrl = url.concat("/bounties/view/").concat(bountyIds[i]);
            bountyUrls.push(bountyUrl);
        }
        console.log("Token [" + token + "]");
        const claimUrl = url.concat("/claims/view/").concat(claimId);
        let addrOutputs = [];

        const linearFee = Cardano.LinearFee.new(
            Cardano.BigNum.from_str(minFeeA),
            Cardano.BigNum.from_str(minFeeB)
        )
        const txConfig = Cardano.TransactionBuilderConfigBuilder.new()
            .coins_per_utxo_word(Cardano.BigNum.from_str(coinsPerUtxoWord))
            .fee_algo(linearFee)
            .key_deposit(Cardano.BigNum.from_str(keyDeposit))
            .pool_deposit(Cardano.BigNum.from_str(poolDeposit))
            .max_tx_size(+maxTxSize)
            .max_value_size(+maxValSize)
            .prefer_pure_change(true)
            .build();

        const inputs = req.query.input.split(",");
        const allUTXOs = Cardano.TransactionUnspentOutputs.new();
        for (let i = 0; i < inputs.length; ++i) {
            if ((inputs[i] != null) && (inputs[i] !== '')) {
                try {
                    const txUnspentOutput = Cardano.TransactionUnspentOutput.from_hex(inputs[i]);
                    allUTXOs.add(txUnspentOutput);
                } catch (e) {
                }
            }
        }
        let tx = null;
        let errorMsg = null;
        let startIndex = 0;
        let stopIndex = 5;
        if (token !== 'ADA') {
            stopIndex = 5;
            startIndex = 2;
        }
        for (let stratIndex = startIndex; stratIndex < stopIndex; ++stratIndex) {
            txSendAmount = sendAmount;
            addrOutputs = [];
            for (let i = 0; i < 2; ++i) {
                let totalAmountOfADANeeded = 0;
                errorMsg = null;
                console.log("Using stratIndex " + stratIndex + " with i " + i);
                const txBuilder = Cardano.TransactionBuilder.new(txConfig);
                if (token === 'ADA') {
                    totalAmountOfADANeeded += txSendAmount;
                    txBuilder.add_output(
                        Cardano.TransactionOutputBuilder.new()
                            .with_address(Cardano.Address.from_bech32(paymentAddress))
                            .next()
                            .with_value(Cardano.Value.new(Cardano.BigNum.from_str(txSendAmount.toString(10))))
                            .build()
                    );
                    if (stratIndex >= 2) {
                        if ((i >= 1) && (addrOutputs.length > 0)) {
                            for (let idx = 0; idx < addrOutputs.length; ++idx) {
                                console.log("Processing extra output for [" + addrOutputs[idx].address().to_json() + "]");
                                let mytxOutputBuilder = Cardano.TransactionOutputBuilder.new();
                                mytxOutputBuilder = mytxOutputBuilder.with_address(addrOutputs[idx].address());
                                let mytxOutputAmountBuilder = mytxOutputBuilder.next();
                                if (addrOutputs[idx].amount().multiasset() != null) {
                                    if (paymentAddress === addrOutputs[idx].address()) {
                                        console.log("Processing paymentAddress [" + paymentAddress + "] output");
                                    }
                                    let minUTxoVal = ((160 + addrOutputs[idx].to_bytes().length) * coinsPerUtxoWord);
                                    console.log("Calculated minUTxoVal [" + minUTxoVal + "]");
                                    if ((minUTxoVal < 1500000)
                                        || (paymentAddress === addrOutputs[idx].address())) {
                                        minUTxoVal = 1500000;
                                    }
                                    totalAmountOfADANeeded += minUTxoVal;
                                    mytxOutputAmountBuilder = mytxOutputAmountBuilder.with_coin_and_asset(Cardano.BigNum.from_str(minUTxoVal.toFixed(0)),
                                        addrOutputs[idx].amount().multiasset());
                                } else {
                                    totalAmountOfADANeeded += 1500000;
                                    mytxOutputAmountBuilder = mytxOutputAmountBuilder.with_coin(Cardano.BigNum.from_str("1500000"));
                                }
                                txBuilder.add_output(mytxOutputAmountBuilder.build());
                            }
                        }
                    }
                } else {
                    let txOutputBuilder = Cardano.TransactionOutputBuilder.new();
                    txOutputBuilder = txOutputBuilder.with_address(Cardano.Address.from_bech32(paymentAddress));
                    let txOutputAmountBuilder = txOutputBuilder.next();
                    const multiAsset = Cardano.MultiAsset.new()
                    const assets = Cardano.Assets.new()
                    assets.insert(
                        Cardano.AssetName.new(Buffer.from(hexTokenName, "hex")),
                        Cardano.BigNum.from_str(sendAmount.toString(10))
                    );
                    multiAsset.insert(
                        Cardano.ScriptHash.from_bytes(Buffer.from(hexPolicyId, "hex")),
                        assets
                    );
                    totalAmountOfADANeeded += 1500000;
                    txOutputAmountBuilder = txOutputAmountBuilder.with_coin_and_asset(Cardano.BigNum.from_str("1500000"), multiAsset);
                    txBuilder.add_output(txOutputAmountBuilder.build());
                    console.log("Verify outputs [" + addrOutputs.length + "]");
                    if ((i >= 1) && (addrOutputs.length > 0)) {
                        for (let idx = 0; idx < addrOutputs.length; ++idx) {
                            try {
                                console.log("Processing extra output for [" + addrOutputs[idx].address().to_json() + "]");
                                let mytxOutputBuilder = Cardano.TransactionOutputBuilder.new();
                                mytxOutputBuilder = mytxOutputBuilder.with_address(addrOutputs[idx].address());
                                let mytxOutputAmountBuilder = mytxOutputBuilder.next();
                                if (addrOutputs[idx].amount().multiasset() != null) {
                                    if (paymentAddress === addrOutputs[idx].address()) {
                                        console.log("Processing paymentAddress [" + paymentAddress + "] output");
                                    }
                                    let minUTxoVal = ((160 + addrOutputs[idx].to_bytes().length) * coinsPerUtxoWord);
                                    console.log("Calculated minUTxoVal [" + minUTxoVal + "]");
                                    if ((minUTxoVal < 1500000)
                                        || (paymentAddress === addrOutputs[idx].address())) {
                                        minUTxoVal = 1500000;
                                    }
                                    totalAmountOfADANeeded += minUTxoVal;
                                    mytxOutputAmountBuilder = mytxOutputAmountBuilder.with_coin_and_asset(Cardano.BigNum.from_str(minUTxoVal.toFixed(0)),
                                        addrOutputs[idx].amount().multiasset());
                                } else {
                                    totalAmountOfADANeeded += 1500000;
                                    mytxOutputAmountBuilder = mytxOutputAmountBuilder.with_coin(Cardano.BigNum.from_str("1500000"));
                                }
                                txBuilder.add_output(mytxOutputAmountBuilder.build());
                            } catch (e) {
                                console.log("Could not process output [" + idx + "]");
                                console.log(e);
                                errorMsg = "TXERROR";
                            }
                        }
                    }
                }
                try {
                    txBuilder.set_ttl_bignum(Cardano.BigNum.from_str((+slot + ttl).toFixed(0)));
                } catch (e) {
                    console.log("set_ttl");
                    console.log(e);
                    errorMsg = "TTLERROR";
                }

                if (stratIndex < 4) {
                    if (errorMsg == null) {
                        try {
                            txBuilder.add_inputs_from(allUTXOs, stratIndex);
                            if (i === 0) {
                                txBuilder.add_change_if_needed(Cardano.Address.from_hex(changeAdd));
                                const tempTx = txBuilder.build_tx();
                                console.log(tempTx);
                                const txOutputs = tempTx.body().outputs();
                                for (let idx = 0; idx < txOutputs.len(); ++idx) {
                                    const txOutput = txOutputs.get(idx);
                                    const coins = +txOutput.amount().coin().to_str();
                                    const multiAsset = txOutput.amount().multiasset();
                                    if ((coins < 1500000)
                                        && (multiAsset != null)) {
                                        addrOutputs.push(txOutput);
                                        console.log("Multiasset below 1.5 ADA. Add to Addr Output Check.");
                                    }
                                }
                            }
                        } catch (e) {
                            console.error(e);
                            errorMsg = "NOTENOUGHADA";
                        }
                    }
                } else if (errorMsg == null) {
                    if (token === 'ADA') {
                        let found = false;
                        for (let k = 0; k < allUTXOs.len(); ++k) {
                            try {
                                const txUnspentOutput = allUTXOs.get(k);
                                const multiAsset = txUnspentOutput.output().amount().multiasset();
                                if ((multiAsset == null) && (+txUnspentOutput.output().amount().coin().to_str() > amtCheck)) {
                                    found = true;
                                    txBuilder.add_input(
                                        txUnspentOutput.output().address(),
                                        txUnspentOutput.input(),
                                        txUnspentOutput.output().amount()
                                    );
                                    break;
                                }
                            } catch (e) {
                                console.error(e);
                            }
                        }

                        if (!found) {
                            let total = 0;
                            // increasing the amount we're looking for since we couldn't find enough without multiassets
                            amtCheck += 3500000;
                            for (let k = 0; k < allUTXOs.len(); ++k) {
                                try {
                                    const txUnspentOutput = allUTXOs.get(k);
                                    const amt = +txUnspentOutput.output().amount().coin().to_str();
                                    const multiAsset = txUnspentOutput.output().amount().multiasset();
                                    if ((multiAsset == null)
                                        && (amt > 3000000)) {
                                        total += amt;
                                        txBuilder.add_input(
                                            txUnspentOutput.output().address(),
                                            txUnspentOutput.input(),
                                            txUnspentOutput.output().amount()
                                        );
                                        if (total >= amtCheck) {
                                            found = true;
                                            break;
                                        }
                                    } else if ((multiAsset != null)
                                        && (amt > 5000000)) {
                                        total += amt;
                                        txBuilder.add_input(
                                            txUnspentOutput.output().address(),
                                            txUnspentOutput.input(),
                                            txUnspentOutput.output().amount()
                                        );
                                        if (total >= amtCheck) {
                                            found = true;
                                            break;
                                        }
                                    }
                                } catch (e) {
                                    console.log(e);
                                }
                            }
                        }
                        if (!found) {
                            console.error("Could not manually add inputs for a transaction");
                            errorMsg = "MANUALTXERROR";
                        }
                    } else {
                        let total = 0;
                        let totalCoin = 0;
                        for (let k = 0; k < allUTXOs.len(); ++k) {
                            try {
                                const txUnspentOutput = allUTXOs.get(k);
                                const amt = +txUnspentOutput.output().amount().coin().to_str();
                                const multiAsset = txUnspentOutput.output().amount().multiasset();
                                const usedInput = [];
                                if (multiAsset != null) {
                                    const sHashes = multiAsset.keys();
                                    for (let j = 0; j < sHashes.len(); ++j) {
                                        const sHash = sHashes.get(j);
                                        const assets = multiAsset.get(sHash);
                                        const numAsset = assets.get(assetName);
                                        if (numAsset != null) {
                                            console.log(numAsset.to_str());
                                            total += (+numAsset.to_str());
                                            totalCoin += amt;
                                            console.log("Using input [" + txUnspentOutput.input().to_json() + "]");
                                            usedInput.push(txUnspentOutput.input().to_json());
                                            txBuilder.add_input(
                                                txUnspentOutput.output().address(),
                                                txUnspentOutput.input(),
                                                txUnspentOutput.output().amount()
                                            );
                                            if (total > sendAmount) {
                                                console.log("Found enough [" + token + "] for the tx");
                                                break;
                                            }
                                        }
                                    }
                                    if (total > sendAmount) {
                                        console.log("Finished inputs. Checking totalAmountOfADANeeded [" + totalAmountOfADANeeded + "] vs totalCoin [" + totalCoin + "]");
                                        if (totalAmountOfADANeeded < 4000000) {
                                            totalAmountOfADANeeded = 4000000;
                                        }
                                        if (totalCoin < totalAmountOfADANeeded) {
                                            console.log("Too little ADA in the TX.. looking for ADA only UTXO");
                                            for (let l = 0; l < allUTXOs.len(); ++l) {
                                                const testUtxo = allUTXOs.get(l);
                                                const input = testUtxo.input().to_json();
                                                let skip = false;
                                                for (let m = 0; m < usedInput.length; ++m) {
                                                    if (usedInput[m] === input) {
                                                        skip = true;
                                                        break;
                                                    }
                                                }
                                                if (!skip) {
                                                    const multiAsset = testUtxo.output().amount().multiasset();
                                                    const amt = +testUtxo.output().amount().coin().to_str();
                                                    if ((multiAsset == null) && (amt > 4000000)) {
                                                        totalCoin += amt;
                                                        console.log("Adding [" + testUtxo.input().to_json() + "] with [" + amt + "] ada");
                                                        usedInput.push(testUtxo.input().to_json());
                                                        txBuilder.add_input(
                                                            testUtxo.output().address(),
                                                            testUtxo.input(),
                                                            testUtxo.output().amount()
                                                        );
                                                        if (totalCoin > totalAmountOfADANeeded) {
                                                            console.log("totalCoin [" + totalCoin + "] >= totalAmountOfADANeeded [" + totalAmountOfADANeeded + "]");
                                                            break;
                                                        }
                                                    }
                                                } else {
                                                    console.log("Skipping already used address [" + input + "]");
                                                }
                                            }
                                            if (totalCoin < totalAmountOfADANeeded) {
                                                console.log("Need to check addresses with multi-assets to get the amount of ADA needed");
                                                for (let l = 0; l < allUTXOs.len(); ++l) {
                                                    const testUtxo = allUTXOs.get(l);
                                                    const input = testUtxo.input().to_json();
                                                    let skip = false;
                                                    for (let m = 0; m < usedInput.length; ++m) {
                                                        if (usedInput[m] === input) {
                                                            skip = true;
                                                            break;
                                                        }
                                                    }
                                                    if (!skip) {
                                                        const multiAsset = testUtxo.output().amount().multiasset();
                                                        const amt = +testUtxo.output().amount().coin().to_str();
                                                        if ((multiAsset != null) && (amt > 8000000)) {
                                                            totalCoin += amt;
                                                            console.log("Adding [" + testUtxo.input().to_json() + "] with [" + amt + "] ada");
                                                            usedInput.push(testUtxo.input().to_json());
                                                            txBuilder.add_input(
                                                                testUtxo.output().address(),
                                                                testUtxo.input(),
                                                                testUtxo.output().amount()
                                                            );
                                                            if (totalCoin > totalAmountOfADANeeded) {
                                                                console.log("totalCoin [" + totalCoin + "] >= totalAmountOfADANeeded [" + totalAmountOfADANeeded + "]");
                                                                break;
                                                            }
                                                        }
                                                    } else {
                                                        console.log("Skipping already used address [" + input + "]");
                                                    }
                                                }
                                            }
                                        }
                                        break;
                                    }
                                }
                            } catch (e) {
                                console.log(e);
                            }
                        }
                    }
                }
                if (errorMsg == null) {
                    if (claimId === '') {
                        const json = JSON.parse("{}");
                        for (let i = 0; i < bountyUrls.length; ++i) {
                            const jsonEntry = {
                                "bounty_hash": hashes[i],
                                "bounty_url": bountyUrls[i]
                            };
                            json[i] = jsonEntry;
                        }
                        txBuilder.add_json_metadatum(Cardano.BigNum.from_str("1"), JSON.stringify(json));
                    } else {
                        const json = JSON.stringify({
                            "claim_url": claimUrl,
                            "bounty_url": bountyUrls[0]
                        });
                        txBuilder.add_json_metadatum(Cardano.BigNum.from_str("1"), json);
                    }
                    try {
                        txBuilder.add_change_if_needed(Cardano.Address.from_hex(changeAdd));
                    } catch (e) {
                        try {
                            console.log(e.toString());
                            const errorString = e.toString();
                            if (errorString.indexOf("Insufficient input") >= 0) {
                                // Insufficient input in transaction. shortage: {ada in inputs: 4000000, ada in outputs: 4500000, fee 211173
                                errorMsg = "INSUFFICIENT";
                            } else if (errorString.indexOf("Not enough ADA leftover") >= 0) {
                                // Not enough ADA leftover to include non-ADA assets in a change address
                                errorMsg = "CHANGEADDR";
                            }
                        } catch (e2) {
                            errorMsg = "TXERROR";
                        }
                    }
                }
                if (errorMsg == null) {
                    // for bounties, subtract out the fee
                    // for claims, the bounty owner pays the fees
                    if ((i === 0) && (claimId === '')) {
                        // for bounties, subtract out the fee
                        // for claims, the bounty owner pays the fees
                        if (i === 0) {
                            const fee = txBuilder.get_fee_if_set();
                            console.log("Fee [" + fee.to_str() + "]");
                            txSendAmount = txSendAmount - (+fee.to_str());
                            console.log("New send amount [" + txSendAmount + "]");
                        }
                    } else if (i === 0) {
                        console.log("Testing tx..");
                        tx = txBuilder.build_tx();
                        console.log(tx.to_json());
                        const txOutputs = tx.body().outputs();
                        for (let j = 0; j < txOutputs.len(); ++j) {
                            const txOutput = txOutputs.get(j);
                            const coins = +txOutput.amount().coin().to_str();
                            if (coins < 1500000) {
                                addrOutputs.push(txOutput);
                                console.log("Multiasset below 1.5 ADA. Add to Addr Output Check.");
                            }
                        }
                    } else if (i > 0) {
                        console.log("Creating tx..");
                        tx = txBuilder.build_tx();
                        console.log(tx.to_json());
                        if (stratIndex < 4) {
                            const txOutputs = tx.body().outputs();
                            for (let j = 0; j < txOutputs.len(); ++j) {
                                const txOutput = txOutputs.get(j);
                                const coins = +txOutput.amount().coin().to_str();
                                if (coins < 1500000) {
                                    errorMsg = "BABBAGE";
                                    console.error("TX would error out because of Babbage UTXO [" + coins + "] Output below [" + txSendAmount + "]");
                                    tx = null;
                                }
                            }
                        }
                        if (tx != null) {
                            stratIndex = 5;
                        }
                    }
                } else {
                    console.log("Error? [" + errorMsg + "]");
                }
            }
        }
        if (tx != null) {
            const date_time_ms = new Date().getTime();
            console.log(tx.to_json());
            if (claimId === '') {
                txSendAmountMap.set(bountyIds[0], txSendAmount.toString(10));
                console.log("Save unsigned TX [" + bountyIds[0] + "]");
                txMap.set(bountyIds[0], tx);
                txTimeMap.set(bountyIds[0], date_time_ms);
            } else {
                txSendAmountMap.set(claimId, txSendAmount.toString(10));
                console.log("Save unsigned TX [" + claimId + "]");
                txMap.set(claimId, tx);
                txTimeMap.set(claimId, date_time_ms);
            }
            res.end(tx.to_hex());
        } else {
            console.error("Could not create a valid transaction with the given wallet");
            if (errorMsg == null) {
                errorMsg = "TXERROR";
            } else if (errorMsg === 'BABBAGE') {
                console.log(req.query);
            }
            res.end(errorMsg);
        }
    } catch (e) {
        console.log(e);
        try {
            res.end("TXERROR");
        } catch (e2) {
            console.log(e2);
        }
    }
})
app.put('/signTx', (req, res) => {
    try {
        const claimId = req.query.claimId;
        const bountyId = req.query.bountyId.split(",")[0];
        const witness = req.query.witness;
        const submitUrl = req.query.submitUrl;
        const projectId = req.query.projectId;
        let unsignedTx;
        let sendAmount;
        if (claimId === '') {
            console.log("Signing [" + bountyId + "]");
            unsignedTx = txMap.get(bountyId);
            sendAmount = txSendAmountMap.get(bountyId);
        } else {
            console.log("Signing [" + claimId + "]");
            unsignedTx = txMap.get(claimId);
            sendAmount = txSendAmountMap.get(claimId);
        }
        if (unsignedTx != null) {
            const txBody = unsignedTx.body();
            const signedTx = Cardano.Transaction.new(
                txBody,
                Cardano.TransactionWitnessSet.from_hex(witness),
                unsignedTx.auxiliary_data()
            );
            console.log(signedTx.to_json());
            axios({
                method: "post",
                url: submitUrl,
                data: Buffer.from(signedTx.to_bytes(), "utf8"),
                headers: {
                    "Content-Type": "application/cbor",
                    "project_id": projectId
                }
            }).then(result => {
                console.log("Submit success [" + result.data + "]");
                res.end(sendAmount.concat(":").concat(result.data));
            }).catch(e => {
                console.log("Submit failed");
                console.log(e);
                let errorMsg = "SIGSUBMITEX";
                if ((e != null) && (e.response != null) &&
                    (e.response.data != null) && (e.response.data.message != null)
                    && (e.response.data.message.indexOf('ValueNotConservedUTxO') > 0)) {
                    errorMsg = "NOTCONSERVED";
                } else if ((e != null) && (e.response != null) &&
                    (e.response.data != null) && (e.response.data.message != null)
                    && (e.response.data.message.indexOf('BabbageOutputTooSmallUTxO') > 0)) {
                    errorMsg = "BABBAGE";
                } else if ((e != null) && (e.response != null) && (((e.response.status != null) && ((e.response.status === 400) || (e.response.status === 404)))
                    || ((e.response.statusText != null) && (e.response.statusText === 'Not Found')))) {
                    errorMsg = "404";
                } else if ((e != null) && (e.code != null) && (e.code === 'ECONNRESET')) {
                    errorMsg = "ECONNRESET";
                }
                res.end(errorMsg);
                if (errorMsg !== "404") {
                    if (claimId === '') {
                        txMap.delete(bountyId);
                        txTimeMap.delete(bountyId);
                        txSendAmountMap.delete(bountyId);
                    } else {
                        txMap.delete(claimId);
                        txTimeMap.delete(claimId);
                        txSendAmountMap.delete(claimId);
                    }
                }
            });
        } else {
            console.error("TX NOT FOUND");
            res.end("TXNOTFOUND");
        }
    } catch (e) {
        console.log(e);
        res.end("SIGEX");
    }
});
app.listen(port, async () => {
    try {
        retrieveSecrets().then(secretsString => {
            //write to .env file at root level of project:
            fs.writeFile('/opt/nodeserver/.env', secretsString).then(val => {
                try {
                    require('dotenv').config(({path: path.resolve('/opt/nodeserver/.env')}));
                    url = process.env['url'];
                } catch (e) {
                    console.log("ENV ERROR [" + e + "]");
                    process.exit(-1);
                }
            }).catch(e => {
                console.log("ERROR [" + e + "]");
                console.log(e);
                process.exit(-1);
            });
        });
        console.log(`Server is listening on port ${port}`);
    } catch (e) {
//log the error and crash the app
        console.log("Error in setting environment variables", e);
        process.exit(4);
    }
});
app.use(function (err, req, res, next) {
    if ((err != null) && (err.stack != null)) {
        console.error(err.stack);
        res.status(500).send('Uncaught Exception');
    } else {
        console.error(`Uncaught Exception: ${err}`);
        res.status(500).send('Uncaught Exception');
    }
});

process.on('beforeExit', code => {
    // Can make asynchronous calls
    setTimeout(() => {
        console.log(`Process will exit with code: ${code}`);
        process.exit(code);
    }, 100);
});
process.on('exit', code => {
    // Only synchronous calls
    console.log(`Process exited with code: ${code}`);
});
process.on('SIGTERM', signal => {
    console.log(`Process ${process.pid} received a SIGTERM signal`);
    process.exit(0);
});
process.on('SIGINT', signal => {
    console.log(`Process ${process.pid} has been interrupted`);
    process.exit(0);
});
process.on('uncaughtException', err => {
    if ((err != null) && (err.message != null)) {
        console.error(`Uncaught Exception: ${err.message}`);
        process.exit(1);
    } else {
        console.error(`Uncaught Exception: ${err}`);
        process.exit(1);
    }
});
process.on('unhandledRejection', (reason, promise) => {
    if ((err != null) && (err.message != null)) {
        console.error('Unhandled rejection at ', promise, `reason: ${err.message}`);
        process.exit(3);
    } else {
        console.error(`Uncaught rejection: ${err}`);
        process.exit(3);
    }
});
// Add a health check route in express
app.get('/_health', (req, res) => {
    const minusHour = new Date().getTime() - 3600000;
    let keys = [...txTimeMap.keys()];
    for (let i = 0; i < keys.length; ++i) {
        if (txTimeMap.has(keys[i])) {
            const time = txTimeMap.get(keys[i]);
            if (time < minusHour) {
                txMap.delete(keys[i]);
                txTimeMap.delete(keys[i]);
                txSendAmountMap.delete(keys[i]);
            }
        }
    }
    res.status(200).send('OK')
})
