#!/bin/bash
mkdir -p dist
cp -r node_modules dist
cp server.js dist
cp package* dist
cp adabounties-nodeserver.service dist
node_modules/node-deb/node-deb -- ./dist/*
# ls -1 | grep -P "adabounties.*deb" | xargs debsigs --sign=origin -k $GPGKEY
ls -1 | grep -P "adabounties-nodeserver.*deb"

